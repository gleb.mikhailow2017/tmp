using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Compression;

public class bullet : MonoBehaviour
{
    [SyncVar] public float damage;
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.GetComponent<PlayerController>()) collision.gameObject.GetComponent<PlayerController>().health -= damage;
        Destroy(gameObject);
    }
}
